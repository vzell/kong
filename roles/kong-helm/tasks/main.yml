---

- name: Create namespace
  kubernetes.core.k8s:
    state: present
    api_version: v1
    kind: Namespace
    name: "{{ kong_namespace }}"
  tags:
    - create_namespace

- name: Create TLS secret for hybrid mode
  kubernetes.core.k8s:
    state: present
    definition:
      apiVersion: v1
      kind: Secret
      metadata:
        name: "{{ kong_cluster_cert_secret_name }}"
        namespace: "{{ kong_namespace }}"
        type: kubernetes.io/tls
      data:
        tls.crt: "{{ lookup('file', kong_env ~ '/cluster.crt') | b64encode }}"
        tls.key: "{{ lookup('file', kong_env ~ '/cluster.key') | b64encode }}"
  tags:
    - create_tls_secret

- name: Deploy chart
  kubernetes.core.helm:
    release_name: "{{ kong_release_name }}"
    chart_ref: "{{ kong_chart_ref }}"
    chart_version: "{{ kong_chart_version }}"
    release_namespace: "{{ kong_namespace }}"
    create_namespace: true
    values: "{{ lookup('template', values_file) | from_yaml }}"
  tags:
    - deploy_chart

- name: Render helm chart templates locally
  block:
    - name: Create template directory
      file:
        path: "{{ item }}"
        state: directory
        mode: '0755'
      loop:
        - "{{ kong_template_dir }}/{{ kong_env }}/{{ plane }}"

    - name: Create values file
      template:
        src: "{{ values_file }}"
        dest: "{{ kong_template_dir }}/{{ kong_env }}/{{ plane }}/values.yaml"
        mode: '0644'

    - name: Render chart templates
      command: |
        helm template {{ kong_release_name }} \
          {{ kong_chart_ref }} \
          -f {{ kong_template_dir }}/{{ kong_env }}/{{ plane }}/values.yaml \
          -n {{ kong_namespace }} \
          --output-dir {{ kong_template_dir }}/{{ kong_env }}/{{ plane }} \
          --kubeconfig {{ kong_kubeconfig }} \
          --kube-version 1.21 \
          --api-versions networking.k8s.io/v1/Ingress
      changed_when: false
  when:
    - helm_render is defined
    - helm_render | bool
